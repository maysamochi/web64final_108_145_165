-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 06:09 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Vaccine`
--

-- --------------------------------------------------------

--
-- Table structure for table `Registration`
--

CREATE TABLE `Registration` (
  `RegistrationID` int(100) NOT NULL,
  `RegistrationName` varchar(200) NOT NULL,
  `RegistrationSurname` varchar(200) NOT NULL,
  `VaccineID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Registration`
--

INSERT INTO `Registration` (`RegistrationID`, `RegistrationName`, `RegistrationSurname`, `VaccineID`) VALUES
(1, 'Tinnapob', 'Kitwiriyakul', 2),
(2, 'Yalinda', 'Bobthong', 1),
(3, 'Tinnapob', 'Kitwiriyakul', 1),
(4, 'Tinnapob', 'Kitwiriyakul', 2),
(5, 'Tinnapob', 'Kitwiriyakul', 3),
(6, 'Tinnapob', 'Kitwiriyakul', 4);

-- --------------------------------------------------------

--
-- Table structure for table `VaccineType`
--

CREATE TABLE `VaccineType` (
  `VaccineID` int(11) NOT NULL,
  `VaccineName` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `VaccineType`
--

INSERT INTO `VaccineType` (`VaccineID`, `VaccineName`) VALUES
(1, 'Sinovac'),
(2, 'Pfizer'),
(3, 'Moderna'),
(4, 'Astrazeneca');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Registration`
--
ALTER TABLE `Registration`
  ADD PRIMARY KEY (`RegistrationID`),
  ADD KEY `VaccineID` (`VaccineID`);

--
-- Indexes for table `VaccineType`
--
ALTER TABLE `VaccineType`
  ADD PRIMARY KEY (`VaccineID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Registration`
--
ALTER TABLE `Registration`
  MODIFY `RegistrationID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `VaccineType`
--
ALTER TABLE `VaccineType`
  MODIFY `VaccineID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Registration`
--
ALTER TABLE `Registration`
  ADD CONSTRAINT `VaccineID` FOREIGN KEY (`VaccineID`) REFERENCES `VaccineType` (`VaccineID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
