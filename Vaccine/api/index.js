const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10


const mysql = require('mysql')
const connection = mysql.createConnection({
    host:'localhost',
    user:'vaccine_admin',
    password:'vaccine_admin',
    database:'Vaccine'
})

connection.connect ()
const express = require('express')
const app = express()
const port = 6000

app.post("/register_vaccine", (req, res) => {

    let registration_id = req.query.registration_id
    let registration_name = req.query.registration_name
    let registration_surname = req.query.registration_surname
    let vaccine_id = req.query.vaccine_id

    bcrypt.hash(registration_id, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Registration
                    (RegistrationID, RegistrationName, RegistrationSurname, VaccineID) 
                    VALUES ('${registration_id}',
                            '${registration_name}',
                            '${registration_surname}',
                            '${vaccine_id}')`
                            
        console.log(query)

        connection.query( query, (err, rows) => {
            if (err) {
                console.log(err)
                res.json ({
                            "status" : "400",
                            "message" : "Error inserting data into db"
                        })
            }else {
                res.json({
                    "status" : "200",
                    "message" : "Registering succesful"
                })
            } 
        });
    });
});



app.listen(port, () => {
    console.log( `Now starting Running System Backend ${port}`)
 })