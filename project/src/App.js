import logo from './logo.svg';
import './App.css';

import VaccineData from './component/VaccineData';
import Header from './component/Header';
import Footer from './component/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <VaccineData />
      <Footer />
    </div>
  );
}

export default App;