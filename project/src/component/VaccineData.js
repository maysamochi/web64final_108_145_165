import {useState} from "react" ;
function VaccineData (props) {
    const [number , setNumber] = useState("");
    const [name , setName] = useState("");
    const [surname , setSurname] = useState("");
    const [type , setType] = useState("");
function success() {
    let v = document.getElementById("vaccine").value ;
    if(v == 1 ){
    alert("ลงทะเบียนรับวัคซีน Sinovac เรียบร้อยแล้ว")}
    else if (v == 2){
    alert("ลงทะเบียนรับวัคซีน Pfizer เรียบร้อยแล้ว")}
    else if (v == 3){
    alert("ลงทะเบียนรับวัคซีน Moderna เรียบร้อยแล้ว")}
    else if (v == 4){
    alert("ลงทะเบียนรับวัคซีน AstraZeneca เรียบร้อยแล้ว")}
  }
    return(
        <div sx={{ textAlign : 'center'}}>
            <h3>เลขบัตรประจำตัวประชาชน &nbsp; <input type = "text" 
                                                   value = {number}
                                                   onChange = {(e) => {setNumber(e.target.value);} } /> </h3>
            <h3>ชื่อ &nbsp; <input type = "text" 
                                value = {name}
                                onChange = {(e) => {setName(e.target.value);} } /> </h3>
            <h3>นามสกุล &nbsp; <input type = "text" 
                                    value = {surname}
                                    onChange = {(e) => {setSurname(e.target.value);} } /> </h3>
            <h3>กรุณาเลือกหมายเลขของวัคซีนที่ท่านต้องการลงทะเบียน </h3>
            <h4>1.Sinovac</h4>
            <h4>2.Pfizer</h4>
            <h4>3.Moderna</h4>
            <h4>4.AstraZeneca
                <br></br><br></br><input type = "text" 
                                        id = "vaccine"
                                        value = {type}
                                        onChange = {(e) => {setType(e.target.value);} } /></h4>
            <button onClick={success}>ลงทะเบียน</button>
        </div>
    );
}
export default VaccineData;